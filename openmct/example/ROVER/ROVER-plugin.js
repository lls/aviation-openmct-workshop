
define([
    "./ROVERLimitProvider",
], function (
    ROVERLimitProvider
) {

    function ROVERPlugin() {

        function getROVERDictionary() {
            return fetch('/example/ROVER/ROVERdictionary.json').then(function (response) {
                return response.json();
            });

        }

        // An object provider builds Domain Objects
        var ROVER_objectProvider = {
            get: function (identifier) {
                return getROVERDictionary().then(function (dictionary) {
                    //console.log("ROVER-dictionary-plugin.js: identifier.key = " + identifier.key);
                    if (identifier.key === 'ROVER') {
                        return {
                            identifier: identifier,
                            name: dictionary.name,
                            type: 'folder',
                            location: 'ROOT'
                        };
                    } else {
                        var measurement = dictionary.measurements.filter(function (m) {
                            return m.key === identifier.key;
                        })[0];

                        return {
                            identifier: identifier,
                            name: measurement.name,
                            type: 'ROVER.telemetry',
                            telemetry: {
                                values: measurement.values
                            },
                            location: 'ROVER.taxonomy:ROVER'
                        };
                    }
                });
            }
        };

        // The composition of a domain object is the list of objects it contains, as shown (for example) in the tree for browsing.
        // Can be used to populate a hierarchy under a custom root-level object based on the contents of a telemetry dictionary.
        // "appliesTo"  returns a boolean value indicating whether this composition provider applies to the given object
        // "load" returns an array of Identifier objects (like the channels this telemetry stream offers)
        var ROVER_compositionProvider = {
            appliesTo: function (domainObject) {
                return domainObject.identifier.namespace === 'ROVER.taxonomy'
                    && domainObject.type === 'folder';
            },
            load: function (domainObject) {
                return getROVERDictionary()
                    .then(function (dictionary) {
                        return dictionary.measurements.map(function (m) {
                            return {
                                namespace: 'ROVER.taxonomy',
                                key: m.key
                            };
                        });
                    });
            }
        };

        return function install(openmct) {
            // The addRoot function takes an "object identifier" as an argument
            openmct.objects.addRoot({
                namespace: 'ROVER.taxonomy',
                key: 'ROVER'
            });

            openmct.objects.addProvider('ROVER.taxonomy', ROVER_objectProvider);

            openmct.composition.addProvider(ROVER_compositionProvider);

            openmct.telemetry.addProvider(new ROVERLimitProvider());

            openmct.types.addType('ROVER.telemetry', {
                name: 'ROVER Telemetry Point',
                description: 'Telemetry of ROVER',
                cssClass: 'icon-telemetry'
            });
        };
    }

    return ROVERPlugin;
});
