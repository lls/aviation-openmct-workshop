# Artificial Data provider for the ROVER implementation
# sends artificial data to an specified UDP port

from pymavlink import mavutil
import math
import socket
import time
import threading


UDP_IP = "127.0.0.1" #standard ip udp (localhost)
UDP_PORT_SEND =50022   #chosen port to OpenMCT (same as in telemetry server object)
MESSAGE = "23,567,32,4356,456,132,4353467" #init message
UDP_PORT_RCV =50023 #port to receive commands
stop_threads = False


# create a dictionary for the Rover telemetry Points
dictionary = {
    "flag.armed" : 0,
    "flag.mode" : 0,
    "flag.GPSfixType" : 0,
    "lat" : 0,
    "lng" : 0,
    "heading" : 0,
    "groundSpeed" : 0,
    "groundHeight" : 0,
    "ax" : 0,
    "ay" : 0,
    "az" : 0,
    "batteryRemaining" : 0,
    "batteryVoltage" : 0,
    "rc.ch3" : 0,
    "comErrors" : 0
}


## initiate connection to MAVProxy
# Start a connection listening to a UDP port
the_connection = mavutil.mavlink_connection('udpin:localhost:14550')

try:
    # Wait for the first heartbeat 
    #   This sets the system and component ID of remote system for the link
    the_connection.wait_heartbeat()
    print("Heartbeat from system (system %u component %u)" % (the_connection.target_system, the_connection.target_system))
    time.sleep(2)
    # Once connected, use 'the_connection' to get and send messages
except:
    print('Connection to MAVProxy failed!')



# initiate socket and send first message
sockSend = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # Internet, UDP
try:
    sockSend.sendto(MESSAGE.encode(), (UDP_IP, UDP_PORT_SEND))
except:
    print('Initial message failed!')


# initiate socket for commands
sockRcv = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # Internet, UDP
try:
    sockRcv.bind((UDP_IP, UDP_PORT_RCV)) # bind the socket to the specified ip and port
    sockRcv.setblocking(0) # set the socket on unblocking, so we wont get stuck on sockRcv.recvfrom()
except:
    print('Connecting to Telemetry Server failed!')
    sockRcv.close() # if connection fails retry


def sendToMCT():

    while True:

        if stop_threads:
            break

        msg = the_connection.recv_match(blocking=False)

        if not msg:
            continue

        msg_type = msg.get_type()

        if msg_type == "BAD_DATA":
            continue

        elif msg_type == "HEARTBEAT":
            dictionary["flag.armed"] = (msg.base_mode & mavutil.mavlink.MAV_MODE_FLAG_SAFETY_ARMED)
            dictionary["flag.mode"] = mavutil.mode_string_v10(msg)
                
            
        elif msg_type == "GPS_RAW_INT":
            dictionary["lat"] = msg.lat*1e-7 #convert to degree
            dictionary["lng"] = msg.lon*1e-7
            dictionary["heading"] = msg.cog*1e-2
            dictionary["groundSpeed"] = msg.vel/100
            dictionary["groundHeight"] = msg.alt*1e-3
            dictionary["flag.GPSfixType"] = msg.fix_type
            
        elif msg_type == "SCALED_IMU2":
            dictionary["ax"] = -msg.xacc/1000
            dictionary["ay"] = -msg.yacc/1000
            dictionary["az"] = -msg.zacc/1000
                            
        elif msg_type == "SYS_STATUS":
            dictionary["batteryRemaining"] = msg.battery_remaining
            dictionary["batteryVoltage"] = msg.voltage_battery*1e-3
            dictionary["comErrors"] = msg.errors_comm
            
        elif msg_type == "RC_CHANNELS_SCALED":
            dictionary["rc.ch3"] = msg.chan3_scaled


        timeStamp = time.time()


        #send the message to the openmct telemetry server
        for key, value in dictionary.items():
            MESSAGE = "{},{},{}".format(key, value, timeStamp)
            
            # Pumping out the values
            sockSend.sendto(MESSAGE.encode(), (UDP_IP, UDP_PORT_SEND))

            #print your message for validation and wait for the next loop
            #print(MESSAGE)
                    
        

        # Message for OpenMCT must be the same structure as on the receiving side (telemetrysource)
        # default is: key, value, timestamp in s UDP epoch time
        # time.sleep(0.05)


def sendCommand():

    while True:
        if stop_threads:
            break
        try:
            data, address = sockRcv.recvfrom(1024) # buffer size is 1024 bytes
            print("received message: %s" % data)
        except socket.error:
            pass
        else:
            if data == b':Forward':
                the_connection.mav.manual_control_send(the_connection.target_system,0,0,500,0,0) #unused_axis,steering,throttle,unused_axis,buttons
                print('Forward')
            elif data == b':Stop':
                the_connection.mav.manual_control_send(the_connection.target_system,0,0,0,0,0) #unused_axis,steering,throttle,unused_axis,buttons
                print('Stop')
            elif data == b':Left':
                the_connection.mav.manual_control_send(the_connection.target_system,0,-100,300,0,0) #unused_axis,steering,throttle,unused_axis,buttons
                print('Left')
            elif data == b':Right':
                the_connection.mav.manual_control_send(the_connection.target_system,0,100,300,0,0) #unused_axis,steering,throttle,unused_axis,buttons
                print('Right')
            elif data == b':Auto':
                mode_id = the_connection.mode_mapping()['AUTO']
                
                the_connection.mav.set_mode_send(
                the_connection.target_system,
                mavutil.mavlink.MAV_MODE_FLAG_CUSTOM_MODE_ENABLED,
                mode_id)
                print('Mode Auto Set')
                
            elif data == b':Manual':
                mode_id = the_connection.mode_mapping()['MANUAL']
                
                the_connection.mav.set_mode_send(
                the_connection.target_system,
                mavutil.mavlink.MAV_MODE_FLAG_CUSTOM_MODE_ENABLED,
                mode_id)
                print('Mode Manual Set')
                
            elif data == b':Guided':
                mode_id = the_connection.mode_mapping()['GUIDED']
                
                the_connection.mav.set_mode_send(
                the_connection.target_system,
                mavutil.mavlink.MAV_MODE_FLAG_CUSTOM_MODE_ENABLED,
                mode_id)
                print('Mode Guided Set')
                
            elif data == b':Arm':
                the_connection.mav.command_long_send(
                the_connection.target_system,
                the_connection.target_component,
                mavutil.mavlink.MAV_CMD_COMPONENT_ARM_DISARM,
                0,
                1, 0, 0, 0, 0, 0, 0)
                print("ARMED!")
                
            elif data == b':Disarm':
                the_connection.mav.command_long_send(
                the_connection.target_system,
                the_connection.target_component,
                mavutil.mavlink.MAV_CMD_COMPONENT_ARM_DISARM,
                0,
                0, 0, 0, 0, 0, 0, 0)
                print("DISARMED!")
    
def main():

    global sendMsgfromMAVP
    sendMsgfromMAVP = threading.Thread(target=sendToMCT, daemon=True)
    sendMsgfromMAVP.start()

    global command
    command = threading.Thread(target=sendCommand, daemon=True)
    command.start()

if __name__ == '__main__':
    main()
    while True:
        try: 
            time.sleep(1)
        except:
            print('Exiting...')
            stop_threads = True
            sendMsgfromMAVP.join()
            command.join()
            sockSend.close()
            sockRcv.close()
            print('Done!')
            exit()
