// main file of the telemtry server, comment/uncomment what is needed

//import the objects


var RealtimeServer = require('./realtime-server');
var HistoryServer = require('./history-server');

var HistoryReader = require('./history_reader');
//var EXAMPLE = require('./EXAMPLE');
var ROVER = require('./ROVER');

var expressWs = require('express-ws');
var app = require('express')();
expressWs(app);


// initialize the objects


//var example = new EXAMPLE;
var rover = new ROVER;

//var realtimeServerEXAMPLE = new RealtimeServer(example);
//var historyServerEXAMPLE = new HistoryServer(example);

var realtimeServerROVER = new RealtimeServer(rover);
var historyServerROVER = new HistoryServer(rover);

var historyReader = new HistoryReader;
var historyServerReader = new HistoryServer(historyReader);


// use the objects

//app.use('/EXAMPLERealtime', realtimeServerEXAMPLE);
//app.use('/EXAMPLEHistory', historyServerEXAMPLE);

app.use('/ROVERRealtime', realtimeServerROVER);
app.use('/ROVERHistory', historyServerROVER);


app.use('/HistoryReader', historyServerReader);


// start the server

var port = process.env.PORT || 16969
app.listen(port)
